#include <p24FJ64GA002.h>
// MCU configuration bits
_CONFIG2(0xFAFE);
_CONFIG1(0x377F);
#define LED0 LATAbits.LATA0
#define LED1 LATAbits.LATA1
#define LED2 LATBbits.LATB2
#define LED3 LATBbits.LATB3
#define BTN0 PORTBbits.RB7
#define BTN1 PORTBbits.RB6
#define SW0 PORTBbits.RB9
#define SW1 PORTBbits.RB8

int setPR2 = 0x004C;
int vecPerioada[7], stare, vecPas[7], maxDC[7];
int crtDC = 0;
int x, btn1Apasat, btn0Apasat;

void TimerInit(void);
void InitSunete(void);

void IOInit() {

    AD1PCFG = 0xFFFF; // Configure all pins as digital I/O
    // Button 0
    TRISBbits.TRISB7 = 1; // Configure port as input
    ODCBbits.ODB7 = 1; // External pull-up resistor present,
    // this is open drain
    // Button 1
    TRISBbits.TRISB6 = 1; // Configure port as input
    ODCBbits.ODB6 = 1; // External pull-up resistor present,
    // this is open drain
    // Switch 0
    TRISBbits.TRISB9 = 1; // Configure port as input
    ODCBbits.ODB9 = 0; // Not open drain
    // Switch 1
    TRISBbits.TRISB8 = 1; // Configure port as input
    ODCBbits.ODB8 = 0; // Not open drain
    // LED 0
    TRISAbits.TRISA0 = 0; // Configure port as output
    ODCAbits.ODA0 = 0; // Not open drain
    // LED 1
    TRISAbits.TRISA1 = 0; // Configure port as output
    ODCAbits.ODA1 = 0; // Not open drain
    // LED 2
    TRISBbits.TRISB2 = 0; // Configure port as output
    ODCBbits.ODB2 = 0; // Not open drain
    // LED 3
    TRISBbits.TRISB3 = 0; // Configure port as output
    ODCBbits.ODB3 = 0;

    TimerInit();
   // InitSunete();

}

int main() {

    IOInit();
    InitSunete();
     x = 0;
    crtDC = 0;
    stare = 0;
    btn1Apasat = 0;
    btn0Apasat = 0;


    // Main Loop
    while (1) {
        if (BTN1 == 0) {
            if (btn1Apasat < 120) btn1Apasat++;
        } else btn1Apasat = 0;

        if (BTN0 == 0) {
            if (btn0Apasat < 120) btn0Apasat++;
        } else btn0Apasat = 0;


        if (btn1Apasat == 100)
            if (x < 6) x++;
        if (btn0Apasat == 100) if (x > 0) x--;


        switch (x) {
            case 0:LED0 = 0;
                LED1 = 0;
                LED2 = 0;
                LED3 = 1;
                break;
            case 1:LED0 = 0;
                LED1 = 0;
                LED2 = 1;
                LED3 = 0;
                break;
            case 2:LED0 = 0;
                LED1 = 0;
                LED2 = 1;
                LED3 = 1;
                break;
            case 3:LED0 = 0;
                LED1 = 1;
                LED2 = 0;
                LED3 = 0;
                break;
            case 4:LED0 = 0;
                LED1 = 1;
                LED2 = 0;
                LED3 = 1;
                break;
            case 5:LED0 = 0;
                LED1 = 1;
                LED2 = 1;
                LED3 = 0;
                break;
            case 6:LED0 = 0;
                LED1 = 1;
                LED2 = 1;
                LED3 = 1;
                break;
            default: LED0 = 1;
                LED1 = 1;
                LED2 = 1;
                LED3 = 1;
                break;
        }
    }
    return 0;
}

//Set up Timer, target 2Hz interrupts

void TimerInit(void) {
    RPOR7bits.RP15R = 18;
    OC1CON = 0x0000; // Turn off Output Compare 1 Module
    OC1R = 0x0026; // Initialize Compare Register1 with 0x0026
    OC1RS = 0x0026; // Initialize Secondary Compare Register1 with 0x0026
    OC1CON = 0x0006; // Load new compare mode to OC1CON
    PR2 = 384; // Initialize PR2 with 0x004C

    T2CONbits.TSIDL = 0;
    T2CONbits.TGATE = 0;
    T2CONbits.TCKPS = 0;
    T2CONbits.T32 = 0;
    T2CONbits.TCS = 0;
    IPC1bits.T2IP = 1; // Setup Output Compare 1 interrupt for
    IFS0bits.T2IF = 0; // Clear Output Compare 1 interrupt flag
    IEC0bits.T2IE = 1; // Enable Output Compare 1 interrupts
    T2CONbits.TON = 1; // Start Timer2 with assumed settings
}


void __attribute__((__interrupt__, __shadow__, no_auto_psv)) _T2Interrupt(void) {

    //LED0 = ~LED0;
    switch (stare) {
        case 1:
            crtDC += vecPas[x];
            if (crtDC >= maxDC[x]) {
                stare = 0;
                crtDC = maxDC[x];
            }
            break;
        case 0:
            crtDC -= vecPas[x];
            if (crtDC <= 0) {
                stare = 1;
                crtDC = 0;
            }
            break;
    }

    OC1RS = crtDC;
    IFS0bits.T2IF = 0;
}

void InitSunete(void) {
   
//440.00 Hz A (La)
//493.88 Hz B (Si)
//523.26 Hz C (Do)
//587.32 Hz D (Re)
//659.26 Hz E (Mi)
//698.46 Hz F (Fa)
//784.00 Hz G (Sol)

    //Sunet La
    vecPerioada[0] = 55;
    vecPas[0] = 14;
    maxDC[0] = 192;

    //Sunet Si
    vecPerioada[1] = 49;
    vecPas[1] = 12;
    maxDC[1] = 192;

    //Sunet Do
    vecPerioada[2] = 46;
    vecPas[2] = 12;
    maxDC[2] = 192;

     //Sunet Re
    vecPerioada[3] = 41;
    vecPas[3] = 11;
    maxDC[3] = 192;

     //Sunet Mi
    vecPerioada[4] = 36;
    vecPas[4] = 9;
    maxDC[4] = 192;

     //Sunet Fa
    vecPerioada[5] = 34;
    vecPas[5] = 9;
    maxDC[5] = 192;

     //Sunet Sol
    vecPerioada[6] = 31;
    vecPas[6] = 8;
    maxDC[6] = 192;
}